
<h1> i'm smint
  <img src="https://komarev.com/ghpvc/?username=smintf"/> </h1>

<a href="https://smint.cf" target="_blank"> website</a>  |
<a href="https://discordid.netlify.app/?id=825013442804580404" target="_blank">discord</a>  |
<a href="https://keybase.io/smintf" target="_blank">keybase</a>
<p> </p>
<img width="100" height="100" src="https://raw.githubusercontent.com/egonelbre/gophers/master/.thumb/animation/gopher-dance-long-3x.gif">
<details>
<summary><strong>stats</strong></summary>

#### gh stats
[![summery](https://github-readme-stats.vercel.app/api?username=smintf&show_icons=true&theme=github_dark&count_private=true&hide_border=true)](https://github.com/anuraghazra/github-readme-stats)  

#### streak
[![GitHub Streak](http://github-readme-streak-stats.herokuapp.com?user=smintf&theme=github-dark-blue&hide_border=true)](https://git.io/streak-stats)

#### metrics
![Metrics](https://github.com/smintf/smintf/blob/master/github-metrics.svg)

#### discord
[![Discord Status](https://lanyard.cnrad.dev/api/825013442804580404?theme=dark&animated=true&borderRadius=10px&idleMessage=Do%20The%20Right%20Thing.&hideBadges=true)](https://discord.com/users/825013442804580404)

</details>

<details>
<summary><strong>projects</strong></summary>

#### autolingo
[![autolingo](https://github-readme-stats.vercel.app/api/pin/?username=smintf&repo=autolingo&theme=github_dark)](https://github.com/smintf/autolingo)

#### duohacker
[![duohacker](https://github-readme-stats.vercel.app/api/pin/?username=smintf&repo=duohacker&theme=github_dark)](https://github.com/smintf/duohacker)

#### brh
[![autolingo](https://github-readme-stats.vercel.app/api/pin/?username=smintf&repo=brh&theme=github_dark)](https://github.com/smintf/brh)

#### ach
[![autolingo](https://github-readme-stats.vercel.app/api/pin/?username=smintf&repo=ach&theme=github_dark)](https://github.com/smintf/ach)

</details>
